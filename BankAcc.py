"""
THIS IS A SAMPLE BANK SYSTEM
"""


class BankSystem:
    account_details = {
        123: ['zain', 50000, 'saving'],
        456: ['aleem', 25000, 'current'],
        789: ['azeem', 4000, 'student'],
    }

    def create_account(self):
        accNo = int(input("Enter the account no : "))
        name = input("Enter the account holder name : ")
        # self.type = input("Enter the type of account (Current/Saving/student) : ")
        deposit = int(
            input("Enter The Initial amount(>=25,000 for Saving  >=10,000 for current  < 10,000 for student) :"))
        if deposit < 500:
            print("account balance is not less then 500")
            deposit = int(
                input("Enter The Initial amount(>=25,000 for Saving  >=10,000 for current  < 10,000 for student) :"))

        type = (input("Enter type of account(current/saving/student) : "))

        self.account_details.update({accNo: [name, deposit, type]})

        print("\n\tAccount  Created Successfully")

    def show_account(self):
        accNo = int(input("Enter the account no : "))
        if self.account_details[accNo]:
            name = self.account_details[accNo][0]
            acc_balance = self.account_details[accNo][1]
            acc_type = self.account_details[accNo][2]
            print("name is " + name)
            print("youre account balance is: " + str(acc_balance))
            print("type of account is :" + acc_type)
        else:
            print("invalid account number")

    def deposit_amount(self):
        amount = int(input("enter amount to deposit :"))
        accNo = int(input("Enter account no : "))
        if self.account_details[accNo]:
            dep_amount = self.account_details[accNo][1] + amount
            self.account_details[accNo][1] = dep_amount
            self.account_details.update({accNo: self.account_details[accNo]})
            print("amount deposite succesfully")
            print("\tyour updated balance is " + str(dep_amount))
        else:
            print("invalid account no")

    def withdraw_amount(self):
        accNo = int(input("Enter the account no : "))
        if self.account_details[accNo]:
            wt_amount = int(input("enter amount to withdraw :"))
            wt_amount = self.account_details[accNo][1] - wt_amount
            self.account_details[accNo][1] = wt_amount
            self.account_details.update({accNo: self.account_details[accNo]})

            print("your remaining balence is :" + str(wt_amount))
        else:
            print("invalid pin")

    def deposit_bills(self):
        bill = int(input("enter bill no : "))
        bill_amount = int(input("enter bill amount : "))
        accNo = int(input("Enter the account no : "))
        if self.account_details[accNo]:
            rem_balnc = self.account_details[accNo][1] - bill_amount
            self.account_details[accNo][1] = rem_balnc
            self.account_details.update({accNo: self.account_details[accNo]})
            print("\t Bill deposit successfully ")
            print("your remaining balence is :" + str(rem_balnc))
        else:
            print("invalid account number")

    def account_type(self):
        accNo = int(input("Enter the account no : "))

        if self.account_details[accNo][1] > 25000:
            print("your account is 'SAVING ACCOUNT'")
        elif self.account_details[accNo][1] >= 10000 and self.account_details[accNo][1] < 25000:
            print("your account is 'CURRENT ACCOUNT'")
        elif self.account_details[accNo][1] < 10000:
            print("your account is 'STUDENT ACCOUNT'")


Account = BankSystem()

print("\t\tBANKING SYSTEM")
print("\tENTER '1' TO CREATE NEW ACCOUNT")
print("\tENTER '2' TO DEPOSIT AMOUNT")
print("\tENTER '3' TO WITHDRAW AMOUNT")
print("\tENTER '4' TO CHECK BALANCE")
print("\tENTER '5' TO DEPOSIT BILLS")
print("\tENTER '6' TO CHECK TYPE OF ACCOUNT")
print("\tENTER '7' TO EXIT")
print("\tSelect Your Option (1-7) ")
ch = input()
while ch != 7:
    if ch == '1':
        Account.create_account()
    elif ch == '2':
        Account.deposit_amount()
    elif ch == '3':
        Account.withdraw_amount()
    elif ch == '4':
        Account.show_account()
    elif ch == '5':
        Account.deposit_bills()
    elif ch == '6':
        Account.account_type()
    elif ch == '7':
        print("THANK YOU")
        break
    else:
        print("Invalid choice")

    ch = input("SELECT ANY OPTION : ")
